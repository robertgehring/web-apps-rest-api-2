package assign.etl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.*;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import assign.domain.EavesdropMeeting;

public class Transformer {
	
	Logger logger;
	
	public Transformer() {
		logger = Logger.getLogger("Transformer");		
	}
	
	public List<EavesdropMeeting> transform(Elements elements, String eavesdropUrl, String teamMeetingName, String year) 
	{
		Map<String, List<String>> meetingBins = new HashMap<String, List<String>>();
		
		/* Bin the links up into meetings (by date) in the meetingBins map */
		for(Element element: elements)
		{
			try
			{
				/* Get the meeting and the date seperate from the log type */
				String[] eSplit = element.text().split("\\.", 4);
				String meetingAndDate = eSplit[0] + "." + eSplit[1] + "." + eSplit[2];
				String meetingLogType = eSplit[3];
				
				/* If we havent seen the meeting make a new instance of it in the map */
				if(!meetingBins.containsKey(meetingAndDate))
				{
					List<String> newList = new ArrayList<String>();
					newList.add(meetingLogType);
					meetingBins.put(meetingAndDate, newList);
				}
				/* Seen it before so just add that we have this log type */
				else
				{
					List<String> updateList = meetingBins.get(meetingAndDate);
					updateList.add(meetingLogType);
					meetingBins.put(meetingAndDate, updateList); //TODO probably dont need this
				}
			}
			catch(ArrayIndexOutOfBoundsException e)
			{
				logger.warning("href:" + element.text().toString() + " !outofboundsexception in transformer!");
			}
		}
		
		/* We want to construct an EavesdropMeeting for each meeting taking into account the log type priority */
		List<EavesdropMeeting> meetings = new ArrayList<EavesdropMeeting>();
		
		for(String uniqueMeeting: meetingBins.keySet())
		{
			EavesdropMeeting m = new EavesdropMeeting();
			m.setYear(year);
			m.setTeamMeetingName(teamMeetingName);
			
			List<String> meetingLogs = meetingBins.get(uniqueMeeting);
			if(meetingLogs.contains("log.html"))
			{
				 m.setMeetingName(uniqueMeeting + ".log.html");
			}
			else if(meetingLogs.contains("log.txt"))
			{
				m.setMeetingName(uniqueMeeting + ".log.txt");
			}
			else if(meetingLogs.contains("log"))
			{
				m.setMeetingName(uniqueMeeting + ".log");
			}
			else
			{
				m.setMeetingName(uniqueMeeting + ".txt");
			}
			
			m.setLink(eavesdropUrl + teamMeetingName + "/" + year + "/" + m.getMeetingName());
			
			meetings.add(m);
		}
		
		return meetings;
	}
	
	public static void main(String args[])
	{
		EavesdropReader r = new EavesdropReader("http://eavesdrop.openstack.org/meetings/solum_team_meeting/");
		Transformer t = new Transformer();
		List<EavesdropMeeting> l = t.transform(r.getContents("2014"), "http://eavesdrop.openstack.org/meetings/", "solum_team_meeting", "2014");
		for (EavesdropMeeting m: l)
		{
			System.out.println(m.toString());
		}
	}
}