package assign.etl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class EavesdropReader {
	
	String url;
	Logger logger; 
	
	public EavesdropReader(String url) {
		this.url = url;
		
		logger = Logger.getLogger("EavesdropReader");
	}
	
	/*
	 * Return a map where the contents of map is a single entry:
	 * <this.url, List-of-parsed-entries>
	 */
	public Map<String, List<String>> readData() {
		
		logger.info("Inside readData.");
		
		Map<String, List<String>> data = new HashMap<String, List<String>>();
		
		// Read and parse data from this.url
				
		return data;
	}
	
	public List<String> getYears()
	{
    	Document d;
    	try { d = Jsoup.connect(this.url).get(); }
    	catch (IOException e) { return null; }
    	List<String> years = new ArrayList<String>();
    	for(Element e: d.select("a[href]"))
    	{
    		String elementString = e.text().replace("/", "");
    		if(StringUtils.isNumeric(elementString))
    			years.add(elementString);
    	}
    	return years;
	}
	
	/* Extract the data from eavesdrop */
	public Elements getContents(String year)
	{
    	Document d;
    	try { d = Jsoup.connect(this.url + year).get(); }
    	catch (IOException e) { return null; }
    	return d.select("a[href]");
    }
	
	public static void main(String[] args)
	{
		EavesdropReader r = new EavesdropReader("http://eavesdrop.openstack.org/meetings/solum_team_meeting/");
		for(String year: r.getYears())
		{
			r.getContents(year);
		}
	}
	
}
