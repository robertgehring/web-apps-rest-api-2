package assign.etl;

import java.util.List;
import java.util.Map;

import assign.domain.EavesdropMeeting;
import assign.services.EavesdropService;

public class ETLController {
	
	EavesdropReader reader;
	Transformer transformer;
	DBLoader loader;
	
	public ETLController() {
		transformer = new Transformer();
		loader = new DBLoader();
	}
	
	public static void main(String[] args) {
		ETLController etlController = new ETLController();
		etlController.performETLActions();
	}

	private void performETLActions() {		
		try {
//
//			String source = "http://eavesdrop.openstack.org/meetings/solum/";
//			reader = new EavesdropReader(source);
//			
//			// Read data
//			Map<String, List<String>> data = reader.readData();
//			
//			// Transform data
//			Map<String, List<String>> transformedData = transformer.transform(data);
//			
//			// Load data
//			loader.loadData(transformedData);
			
//			EavesdropService eavesdropService = new EavesdropService();
			Transformer t = new Transformer();
			DBLoader loader = new DBLoader();
			
//			for(String project: eavesdropService.getAllProjects())
//			{
//				EavesdropReader r = new EavesdropReader("http://eavesdrop.openstack.org/meetings/" + project + "/");
//
//				for(String year: r.getYears())
//				{
//					List<EavesdropMeeting> l = t.transform(r.getContents(year), "http://eavesdrop.openstack.org/meetings/", project, year);
//					loader.saveEavesdropMeetings(l);
//				}
//			}
			
			String project = "solum";
			
			EavesdropReader r = new EavesdropReader("http://eavesdrop.openstack.org/meetings/" + project + "/");

			for(String year: r.getYears())
			{
				List<EavesdropMeeting> l = t.transform(r.getContents(year), "http://eavesdrop.openstack.org/meetings/", project, year);
				loader.saveEavesdropMeetings(l);
			}
			
			project = "solum_team_meeting";
			
			r = new EavesdropReader("http://eavesdrop.openstack.org/meetings/" + project + "/");

			for(String year: r.getYears())
			{
				List<EavesdropMeeting> l = t.transform(r.getContents(year), "http://eavesdrop.openstack.org/meetings/", project, year);
				loader.saveEavesdropMeetings(l);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
}
