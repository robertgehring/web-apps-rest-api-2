package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.jboss.resteasy.spi.BadRequestException;
import org.jboss.resteasy.spi.NotFoundException;

import assign.domain.EavesdropMeeting;
import assign.domain.EavesdropMeetings;
import assign.domain.EavesdropMeetingsCount;


/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

@Path("/myeavesdrop")
public class EavesdropSolumResource {
	
	private SessionFactory sessionFactory;
	private EavesdropSolumResource eavesdropDBService;
	private String password;
	private String username;
	private String dburl;
	
	public EavesdropSolumResource(@Context ServletContext servletContext) {		
		dburl = servletContext.getInitParameter("DBURL");
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		//this.eavesdropDBService = new EavesdropDBServiceImpl(dburl, username, password);
		
        sessionFactory = new Configuration()
        .configure() // configures settings from hibernate.cfg.xml
        .buildSessionFactory();
	}
	
	@GET
	@Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() {
		System.out.println("Inside helloworld");
		System.out.println("DB creds are:");
		System.out.println("DBURL:" + dburl);
		System.out.println("DBUsername:" + username);
		System.out.println("DBPassword:" + password);		
		return "Hello world " + dburl + " " + username + " " + password; 		
	}
	
	@GET
	@Path("/meetings/{name}/year/{year}/count")
	@Produces("application/xml")
	public StreamingOutput getProjectCount(@PathParam("name") String name, @PathParam("year") String year) throws Exception
	{	
		/* Make sure the project name and years are valid */
		if (!("SOLUM").equalsIgnoreCase(name) && !("SOLUM_TEAM_MEETING").equalsIgnoreCase(name))
			throw new BadRequestException("Name must be solum or solum_team_meeting");
		
		if(!("2013").equals(year) && !("2014").equals(year) && !("2015").equals(year))
			throw new BadRequestException("Support for only years 2013, 2014, and 2015");
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(EavesdropMeeting.class).
        		add(Restrictions.eq("teamMeetingName", name)).add(Restrictions.eq("year", year)); 

		List<?> meetings = criteria.list();
		
		for(Object o: meetings)
		{
			EavesdropMeeting em = (EavesdropMeeting) o;
		}
		
		final EavesdropMeetingsCount eavesdropMeetingsCount = new EavesdropMeetingsCount();
		eavesdropMeetingsCount.setCount(meetings.size());
		
	    return new StreamingOutput() {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	            outputProject(outputStream, eavesdropMeetingsCount);
	         }
	      };
	}
	
	@GET
	@Path("/meetings/{name}/year/{year}")
	@Produces("application/xml")
	public StreamingOutput getProjectByYear(@PathParam("name") String name, @PathParam("year") String year) throws Exception
	{
		/* Make sure the project name and years are valid */
		if (!("SOLUM").equalsIgnoreCase(name) && !("SOLUM_TEAM_MEETING").equalsIgnoreCase(name))
			throw new BadRequestException("Name must be solum or solum_team_meeting");
		
		if(!("2013").equals(year) && !("2014").equals(year) && !("2015").equals(year))
			throw new BadRequestException("Support for only years 2013, 2014, and 2015");
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(EavesdropMeeting.class).
        		add(Restrictions.eq("teamMeetingName", name)).add(Restrictions.eq("year", year)); 

		List<?> meetingsObjs = criteria.list();
		List<EavesdropMeeting> meetingsCasted = new ArrayList<EavesdropMeeting>();
		
		for(Object o: meetingsObjs)
		{
			EavesdropMeeting em = (EavesdropMeeting) o;
			meetingsCasted.add(em);
		}
		
		final EavesdropMeetings eavesdropMeetings = new EavesdropMeetings();
		eavesdropMeetings.setMeetings(meetingsCasted);
		
	    return new StreamingOutput() {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	            outputProject(outputStream, eavesdropMeetings);
	         }
	      };
	}
	
	
	protected void outputProject(OutputStream os, EavesdropMeetingsCount eavesdropMeetingsCount) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(EavesdropMeetingsCount.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(eavesdropMeetingsCount, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	protected void outputProject(OutputStream os, EavesdropMeetings eavesdropMeetings) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(EavesdropMeetings.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(eavesdropMeetings, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}

}
