package assign.resources;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath; 
import javax.ws.rs.core.Application;


/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

@ApplicationPath("/assignment6")
public class EavesdropSolumApplication extends Application {
	
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> classes = new HashSet<Class<?>>();
		
	public EavesdropSolumApplication() {
	}
	
	@Override
	public Set<Class<?>> getClasses() {
		classes.add(EavesdropSolumResource.class);
		return classes;
	}
	
	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}

