package assign.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class EavesdropService 
{
	private String meetingsUrl = "http://eavesdrop.openstack.org/meetings/";
	
	public List<String> getAllProjects()
	{
    	Document d;
    	try { d = Jsoup.connect(meetingsUrl).get(); }
    	catch (IOException e) { return null; }
    	
    	List<String> projects = new ArrayList<String>();
    	Elements elements = d.select("a[href]");
    	for(int i=5;i<elements.size();i++)
    	{
    		projects.add(elements.get(i).text().replace("/", ""));
    	}
    	
    	return projects;
	}
	
	public static void main(String[] args)
	{
		EavesdropService e = new EavesdropService();
		System.out.println(e.getAllProjects().toString());
	}
}
