package assign.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.GenericGenerator;

@XmlRootElement(name = "meeting")
@XmlType(propOrder={"teamMeetingName", "year", "meetingName", "link"})
public class EavesdropMeeting
{
	/*
	- Column team_meeting_name (varchar (20))

	- Column year (varchar or int – up to you)

	- Column meeting_name (varchar (50))

	- Column link (varchar(1000))
	*/
	
	private Long id;
	private String teamMeetingName;
	private String year;
	private String meetingName;
	private String link;
	
	public EavesdropMeeting() {};
	
	public EavesdropMeeting(String teamMeetingName, String year, 
								String meetingName, String link)
	{
		this.teamMeetingName = teamMeetingName;
		this.year = year;
		this.meetingName = meetingName;
		this.link = link;
	}
	
	@XmlTransient
	public void setId(Long id) { this.id = id; }
	
	public Long getId() { return this.id; }
	
	@XmlElement(name = "team_meeting_name")
	public void setTeamMeetingName(String teamMeetingName) { this.teamMeetingName = teamMeetingName; }
	
	public String getTeamMeetingName() { return this.teamMeetingName; }
	
	@XmlElement(name = "year")
	public void setYear(String year) { this.year = year; }
	
	public String getYear() { return this.year; }
	
	@XmlElement(name = "meeting_name")
	public void setMeetingName(String meetingName) { this.meetingName = meetingName; }
	
	public String getMeetingName() { return this.meetingName; }
	
	@XmlElement(name = "link")
	public void setLink(String link) { this.link = link; }
	
	public String getLink() { return this.link; }
	
	public String toString()
	{
		String s = "-----------\n";
		s += "teamMeetingName: " + teamMeetingName + "\n";
		s += "year: " + year + "\n";
		s += "meetingName: " + meetingName + "\n";
		s += "link: " + link + "\n";
		s += "-----------\n";
		
		return s;
	}
}