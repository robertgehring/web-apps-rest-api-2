package assign.domain;

import java.util.List;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

@XmlRootElement(name = "meetings")
public class EavesdropMeetingsCount 
{		    
//	    private List<EavesdropMeeting> meeting = null;
	    private int count;
	    
//	    public EavesdropMeetings() { this.meeting = new ArrayList<EavesdropMeeting>(); }
//	    
//	    public List<EavesdropMeeting> getMeetings() {
//	    	return meeting;
//	    }
//	    
//	    public void setMeetings(List<EavesdropMeeting> meetings) {
//	    	this.meeting = meetings;
//	    }
	    
		@XmlElement(name = "count")
	    public void setCount(int c) { this.count = c; }
	    
	    public int getCount() { return this.count; }
}