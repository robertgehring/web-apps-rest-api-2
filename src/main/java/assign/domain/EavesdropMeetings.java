package assign.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name = "meetings")
@XmlAccessorType(XmlAccessType.FIELD)
public class EavesdropMeetings 
{
    private List<EavesdropMeeting> meeting = null;
    
    public EavesdropMeetings() { this.meeting = new ArrayList<EavesdropMeeting>(); }
    
    public List<EavesdropMeeting> getMeetings() {
    	return meeting;
    }
    
    public void setMeetings(List<EavesdropMeeting> meetings) {
    	this.meeting = meetings;
    }
//    
//	@XmlElement(name = "count")
//    public void setCount(int c) { this.count = c; }
//    
//    public int getCount() { return this.count; }
}
 