package assign.resources;

import java.io.IOException;
import java.util.Scanner;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.custommonkey.xmlunit.XMLTestCase;
import org.custommonkey.xmlunit.XMLUnit;

public class FunctionalTests extends XMLTestCase
{
	@Test
	public void testSolumn2014Count() throws IOException, SAXException
	{
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		ResteasyClient client = new ResteasyClientBuilder().build();
		Response response = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2014/count/").request().get();

		assertEquals(200, response.getStatus());
		
		/* Get control and test XML to compare */
		Document controlDoc = XMLUnit.buildControlDocument(XmlControls.solum2014CountXML);
		Document testDoc = XMLUnit.buildTestDocument(response.readEntity(String.class));
		
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		
		assertXMLEqual(controlDoc, testDoc);
		response.close();
	}

	@Test
	public void testSolumnTeamMeeting2013Count() throws IOException, SAXException
	{
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		ResteasyClient client = new ResteasyClientBuilder().build();
		Response response = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum_team_meeting/year/2013/count").request().get();

		assertEquals(200, response.getStatus());
		
		/* Get control and test XML to compare */
		Document controlDoc = XMLUnit.buildControlDocument(XmlControls.solumTeamMeeting2013CountXML);
		Document testDoc = XMLUnit.buildTestDocument(response.readEntity(String.class));
		
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		
		assertXMLEqual(controlDoc, testDoc);
		response.close();
	}
	
	@Test
	public void testSolumn2014Projects() throws IOException, SAXException
	{
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		ResteasyClient client = new ResteasyClientBuilder().build();
		Response response = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2014/").request().get();

		assertEquals(200, response.getStatus());
		
		/* Get control and test XML to compare */
		Document controlDoc = XMLUnit.buildControlDocument(XmlControls.solum2014ProjectsXML);
		Document testDoc = XMLUnit.buildTestDocument(response.readEntity(String.class));
		
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		
		assertXMLEqual(controlDoc, testDoc);
		response.close();
	}
	
	@Test
	public void testSolumn2013Count() throws IOException, SAXException
	{
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		ResteasyClient client = new ResteasyClientBuilder().build();
		Response response = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2013/count").request().get();

		assertEquals(200, response.getStatus());
		
		/* Get control and test XML to compare */
		Document controlDoc = XMLUnit.buildControlDocument(XmlControls.solum2013CountXML);
		Document testDoc = XMLUnit.buildTestDocument(response.readEntity(String.class));
		
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		
		assertXMLEqual(controlDoc, testDoc);
		response.close();
	}
}
