package assign.resources;

public class XmlControls 
{
	public static String solum2014CountXML = "<meetings>\n<count>1</count>\n</meetings>";
	
	public static String solumTeamMeeting2013CountXML = "<meetings>\n<count>5</count>\n</meetings>";
	
	public static String solum2014ProjectsXML = "<meetings>\n<meeting>\n<team_meeting_name>solum</team_meeting_name>\n<year>2014</year>\n<meeting_name>solum.2014-07-29-16.03.log.html</meeting_name>\n" +
	                                            "<link>http://eavesdrop.openstack.org/meetings/solum/2014/solum.2014-07-29-16.03.log.html</link>\n</meeting>\n</meetings>";

    public static String solum2013CountXML = "<meetings>\n<count>0</count>\n</meetings>";
}
